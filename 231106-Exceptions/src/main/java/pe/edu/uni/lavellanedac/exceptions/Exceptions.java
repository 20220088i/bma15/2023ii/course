/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.uni.lavellanedac.exceptions;

/**
 *
 * @author user=Luis Avellaneda <luis.avellaneda.c@uni.pe>
 */
public class Exceptions {

    public static void main(String[] args) {
        System.out.println("Exceptions !!!");
        int[] array = {10, 20, 30, 40, 50, 60};
        int i = 0;
        /*
        if (i < array.length) {
            System.out.println(array[i]);
        } else {
            System.out.println("Fuera de los límites");
        }
         */
        try {
            System.out.println(array[i]);
//          int j = 10 / i;
            Function(i);
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Fuera de los límites");
            System.out.println(e);
        } catch (ArithmeticException e) {
            System.out.println("Division entre cero");
            System.out.println(e);
        } catch (Exception e) {
            System.out.println("Default !!!");
            System.out.println(e);
        } finally {
            System.out.println("Cierra la Base datos");
        }
    }

    public static void Function(int i) throws ArithmeticException {
        int j = 10 / i;
    }
}
