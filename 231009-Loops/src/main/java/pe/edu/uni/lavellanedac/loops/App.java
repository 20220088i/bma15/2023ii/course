/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package pe.edu.uni.lavellanedac.loops;

/**
 *
 * @author Luis Avellaneda <luis.avellaneda.c@uni.pe>
 */
public class App {

    public static void main(String[] args) {
        System.out.println("Loop 1");
        for (int i = 0; i <= 10; i++) {
            if (i < 10) {
                System.out.print(i + ", ");
            } else {
                System.out.println(i);
            }
        }
        System.out.println("Loop 2");
        for (int i = 10; i >= 0; i--) {
            System.out.println("i: " + i);
        }
        /*
        System.out.println("Loop 3");
        for (int i = 0; ; i++) {
            System.out.println("i: "+i);
        }
         */
 /*
        A[3, 5]
        A[1, 1], A[1, 2], A[1, 3], A[1, 4], A[1, 5]
        A[2, 1], A[2, 2], A[2, 3], A[2, 4], A[2, 5]
        A[3, 1], A[3, 2], A[3, 3], A[3, 4], A[3, 5]
         */
        System.out.println("Loop 3");
        for (int i = 1; i <= 3; i++) {
            for (int j = 1; j <= 5; j++) {
                if (j < 5) {
                    System.out.print("A[" + i + ", " + j + "],\t ");
                } else {
                    System.out.println("A[" + i + ", " + j + "]");
                }
            }
        }
        System.out.println("While loop 1");
        int i = 1;
        while (i <= 5) {
            System.out.println("i: " + i);
            i++;
        }
        System.out.println("While loop 2");
        int j = 5;
        while (j > 0) {
            System.out.println("j: " + j);
            j--;
        }
        /*
        System.out.println("While loop 3");
        int k=0;
        while (true) {            
            System.out.println("k: "+k);
            k++;
        }
         */

        int a = 1;
        while (a <= 3) {
            int b = 1;
            while (b <= 5) {
                if (b < 5) {
                    System.out.print("A[" + a + ", " + b + "],\t ");
                } else {
                    System.out.println("A[" + a + ", " + b + "]");
                }
                b++;
            }
            a++;
        }

        System.out.println("do while loop");
        int p = 1;
        do {
            int q = 1;
            do {
                if (q < 5) {
                    System.out.print("A[" + p + ", " + q + "],\t ");
                } else {
                    System.out.println("A[" + p + ", " + q + "]");
                }
                q++;
            } while (q <= 5);
            p++;
        } while (p <= 3);
        
    }
}
