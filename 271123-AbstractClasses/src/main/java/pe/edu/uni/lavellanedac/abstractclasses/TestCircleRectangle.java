/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.edu.uni.lavellanedac.abstractclasses;

/**
 *
 * @author Luis Avellaneda <luis.avellaneda.c@uni.pe>
 */
public class TestCircleRectangle {

    public static void main(String[] args) {
        System.out.println("TsetCircleRectangle");
        Circle circle = new Circle(5);
        Rectangle rectangle = new Rectangle(5, 5);
        System.out.println("Circle: " + circle.toString());
        System.out.println("Rectangle: " + rectangle.toString());
        System.out.println("Area of Circle: " + circle.getArea());
        System.out.println("Area of Rectangle: " + rectangle.getArea());
        System.out.println("Perimeter of Circle: " + circle.getPerimeter());
        System.out.println("Perimeter of Rectangle: " + rectangle.getPerimeter());
    }

}
