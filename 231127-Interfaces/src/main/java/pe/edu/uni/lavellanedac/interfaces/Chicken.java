/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.edu.uni.lavellanedac.interfaces;

/**
 *
 * @author Luis Avellaneda <luis.avellaneda.c@uni.pe>
 */
public class Chicken extends Animal implements Edible {

    @Override
    public String sound() {
        return "Chicken: kikiriki";
    }

    @Override
    public String howToEat() {
        return "Chicken: fry it!!!";
    }

}
