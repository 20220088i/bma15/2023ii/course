/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.edu.uni.lavellanedac.interfaces;

/**
 *
 * @author PROFESOR
 */
public class Apple extends Fruit {

    @Override
    public String howToEat() {
        return "Apple: Cook a pie!!";
    }

}
