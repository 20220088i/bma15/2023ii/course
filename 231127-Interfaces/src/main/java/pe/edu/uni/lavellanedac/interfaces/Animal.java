/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.edu.uni.lavellanedac.interfaces;

/**
 *
 * @author Luis Avellaneda <luis.avellaneda.c@uni.pe>
 */
public abstract class Animal {

    private double weight;

    public double getWieght() {
        return weight;
    }

    public void setWieght(double wieght) {
        this.weight = wieght;
    }

    public abstract String sound();

}
