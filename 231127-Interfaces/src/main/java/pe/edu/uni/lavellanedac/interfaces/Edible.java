/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package pe.edu.uni.lavellanedac.interfaces;

/**
 *
 * @author Luis Avellaneda <luis.avellaneda.c@uni.pe>
 */
public interface Edible {

    public String howToEat();
}
