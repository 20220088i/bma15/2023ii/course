/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.edu.uni.lavellanedac.classes;

/**
 *
 * @author Luis Avellaneda <luis.avellaneda.c@uni.pe>
 */
public class TestTV {

    public static void main(String[] args) {
        TV tv1;
        tv1 = new TV();

        tv1.turnOn();

        if (tv1.isOn()) {
            System.out.println("Encendido");
            int channel1 = 120;
            int volume1 = 7;
            tv1.settChannel(channel1);
            tv1.settVolume(volume1);
            System.out.println(tv1.toString());
            tv1.channelUp();
            System.out.println(tv1.toString());

        } else {
            System.out.println("Apagado");
        }
    }
}
