/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.edu.uni.lavellanedac.classes;

/**
 *
 * @author Luis Avellaneda <luis.avellaneda.c@uni.pe>
 */
public class TV {

    int channel;
    int volumeLevel;
    boolean on;

    final int minChannelLevel = 1;
    final int maxChannelLevel = 120;
    final int minVolumeLevel = 1;
    final int maxVolumeLevel = 7;

    public TV() {
        channel = 1;
        volumeLevel = 3;
        on = false;
    }

    public void turnOn() {
        on = true;
    }

    public void turnOff() {
        on = false;
    }

    public void settChannel(int newChannel) {
        channel = newChannel;
    }

    public void settVolume(int newVolumeLevel) {
        volumeLevel = newVolumeLevel;
    }

    public void channelUp() {
        if (on && channel < maxChannelLevel) {
            channel++;
        } else if (channel == maxChannelLevel) {
            channel = minChannelLevel;
        }
    }

    public void channelDown() {
        if (on && channel < maxChannelLevel) {
            channel--;
        } else if (channel == minChannelLevel) {
            channel = maxChannelLevel;
        }
    }

    public void volumeUp() {
        if (on && volumeLevel < maxVolumeLevel) {
            volumeLevel++;
        }
    }

    public void volumeDown() {
        if (on && volumeLevel > minVolumeLevel) {
            volumeLevel--;
        }
    }

    @Override
    public String toString() {
        return "TV{" + "channel=" + channel + ", volumeLevel=" + volumeLevel + ", on=" + on + '}';
    }

    public boolean isOn() {
        return on;
    }
    
}
