/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.edu.uni.lavellanedac.classes;

/**
 *
 * @author PROFESOR
 */
public class Circcle {

    double radius;

    Circcle() {
        this.radius = 1;
    }

    Circcle(double newRadius) {
        this.radius = newRadius;
    }

    double getArea() {
        return Math.PI * this.radius * this.radius;
    }

    double getPerimeter() {
        return 2 * Math.PI * this.radius;
    }

    void setRadius(double newRadius) {
        this.radius = newRadius;
    }

    @Override
    public String toString() {
        return "Circcle{" + "radius=" + radius + '}';
    }
    

}
