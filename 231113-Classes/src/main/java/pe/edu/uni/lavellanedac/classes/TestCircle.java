/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */
package pe.edu.uni.lavellanedac.classes;

/**
 *
 * @author Luis Avellaneda <luis.avellaneda.c@uni.pe>
 */
public class TestCircle {

    public static void main(String[] args) {
        System.out.println("Test Circle!");
        Circcle circle1;
        circle1 = new Circcle();
        System.out.println("Area: " + circle1.getArea());
        System.out.println("Perimeter: " + circle1.getPerimeter());
        circle1.setRadius(3);
        System.out.println("New Area: " + circle1.getArea());
        System.out.println("New Perimeter: " + circle1.getPerimeter());
        Circcle circle2;
        circle2 = new Circcle(2.3);
        System.out.println("Area 2: " + circle2.getArea());
        System.out.println("Perimeter 2: " + circle2.getPerimeter());
        System.out.println("ALL: " + circle2.toString());
        Circcle circle3 = new Circcle();
        double radius3 = 125;
        circle3.setRadius(radius3);
        System.out.println("Area 3: " + circle3.getArea());
        System.out.println("Perimeter 3: " + circle3.getPerimeter());
        System.out.println("ALL: " + circle3.toString());
    }
}
