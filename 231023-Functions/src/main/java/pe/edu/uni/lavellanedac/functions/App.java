/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package pe.edu.uni.lavellanedac.functions;

/**
 *
 * @author Luis Avellaneda<luis.avellaneda.c@uni.pe>
 */
import java.util.Arrays;

public class App {

    public static void main(String[] args) {
        System.out.println("Functions!");
        Function();
        Function("Luis");
        Function("Luis", 18);
        String[] result = {"Hello ", "Luis", ", you ", "are ", "18 ", "years ", "old ", "!!!"};
//        System.out.println(Function(result));
        System.out.println(Arrays.toString(result));
        for (int i = 0; i < result.length; i++) {
            System.out.print(result[i]);
        }
        System.out.println("");
        for (String string : result) {
            System.out.print(string);
        }
        System.out.println("");
        System.out.println("\nsuma: " + Function(1, 2, 3, 4));
    }

    public static void Function() {
        System.out.println("Inside the function !!!");
    }

    public static void Function(String name) {
        System.out.println("Inside de function " + name + " !!!");
    }

    public static void Function(String name, int age) {
        System.out.println("Hello " + name + ", you are " + String.valueOf(age) + " years old !!!");
    }

    public static String[] Function(String[] args) {
        return args;
    }

    public static int Function(int... numbers) {
        int total = 0;
        for (int number : numbers) {
            total += number;
            System.out.print(number + "\t");
        }
        return total;
    }

}
