/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.edu.uni.lavellanedac.arrays;

import java.util.Random;

/**
 *
 * @author Luis Avellaneda <luis.avellaneda.c@uni.pe>
 */
public class ArraySamples {

    public static void main(String[] args) {
        //    new ArraySamples().contarNumeros();
        new ArraySamples().contarLetras();
    }

    private void contarNumeros() {
        System.out.println("Array Samples!!!");
        java.util.Scanner input = new java.util.Scanner(System.in);
        System.out.println("Ingrese un numero de ítems: ");
        int n = input.nextInt();
        double[] numeros = new double[n];
        double suma = 0;
        for (int i = 0; i < n; i++) {
            System.out.print("Ingrese el elemento " + i + ": ");
            numeros[i] = input.nextDouble();
            suma += numeros[i];
        }
        double prom = suma / n;
        System.out.print("[");
        for (int i = 0; i < n; i++) {
            if (i < n - 1) {
                System.out.print(numeros[i] + ", ");
            } else {
                System.out.println(numeros[i] + "]");
            }
        }
        System.out.println("La suma de los elementos es: " + suma);
        System.out.println("El promedio de los elementos es: " + prom);

        //calculando la cantidad de numeros mayores al promedio
        int ctd = 0;
        for (int i = 0; i < n; i++) {
            if (numeros[i] > prom) {
                ctd++;
            }
        }
        System.out.println("La cantidad de numeros mayores al promdeio es: " + ctd);
    }

    private void contarLetras() {
        System.out.println("Contar letras minusculas en un Array!!");
        //inicializando array
        char chars[] = {'h', 'o', 'l', 'a'};
        for (int i = 0; i < chars.length; i++) {
            System.out.print(chars[i]);
        }
        System.out.println("");
        System.out.println(chars[chars.length - 1]);
        System.out.println((int) chars[chars.length - 1]);
        System.out.println((char) ('a' + 25));

        char[] alchar = new char[100];
        for (int i = 0; i < alchar.length; i++) {
            Random r = new Random();
            alchar[i] = (char) (r.nextInt(26) + 'a');
            System.out.print(alchar[i]);
        }
        System.out.println("");
        int[] ctd = new int[26];
        for (int i = 0; i < alchar.length; i++) {
            ctd[alchar[i] - 'a']++;
        }
        for (int i = 0; i < ctd.length; i++) {
            System.out.print((char) ('a' + i) + "," + ctd[i] + " ");
        }
    }
}
