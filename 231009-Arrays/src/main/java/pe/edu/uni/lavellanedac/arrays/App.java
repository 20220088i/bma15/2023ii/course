/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package pe.edu.uni.lavellanedac.arrays;

/**
 *
 * @author Luis Avellaneda <luis.avellaneda.c@uni.pe>
 */
public class App {

    public static void main(String[] args) {
        System.out.println("Arrays!");
        int arraySize = 5;
        double[] styleJavaArray;
        double styleCppArray[];

        styleJavaArray = new double[arraySize];
        styleCppArray = new double[arraySize];

        //[0.0, 0.0, 0.0, 0.0, 0.0]
        styleJavaArray[0] = 5;
        styleJavaArray[1] = 4;
        styleJavaArray[2] = 3;
        styleJavaArray[3] = 2;
        styleJavaArray[4] = 1;
        System.out.println("Visualizando");
        System.out.print("[");
        for (int i = 0; i < styleJavaArray.length; i++) {
            if (i < styleJavaArray.length - 1) {
                System.out.print(styleJavaArray[i] + ", ");
            } else {
                System.out.print(styleJavaArray[i]);
            }
        }
        System.out.println("]");
        System.out.println("Definiendo");
        for (int i = 0; i < styleJavaArray.length; i++) {
            styleJavaArray[i] = i;
        }
        System.out.println("Visualizando");
        System.out.print("[");
        for (int i = 0; i < styleJavaArray.length; i++) {
            if (i < styleJavaArray.length - 1) {
                System.out.print(styleJavaArray[i] + ", ");
            } else {
                System.out.print(styleJavaArray[i]);
            }
        }
        System.out.println("]");

        //inicializando forma 2
        double[] myArray = {1.9, 2.9, 3.4, 3.5, 7.2};
        System.out.println("Visualizando");
        System.out.print("[");
        for (int i = 0; i < myArray.length; i++) {
            if (i < myArray.length - 1) {
                System.out.print(myArray[i] + ", ");
            } else {
                System.out.print(myArray[i]);
            }
        }
        System.out.println("]");

        /*
        //escribir nuevos valores
        java.util.Scanner input = new java.util.Scanner(System.in);
        System.out.println("Ingrese " + myArray.length + " valores:");
        for (int i = 0; i < myArray.length; i++) {
            myArray[i] = input.nextDouble();
        }
        for (int i = 0; i < myArray.length; i++) {
            System.out.println("myArray[" + i + "]: " + myArray[i]);
        }
         */
        //inicializando valores aleatorios
        for (int i = 0; i < myArray.length; i++) {
            myArray[i] = (int) (Math.random() * 100);
        }
        System.out.println("Visualizando");
        System.out.print("[");
        for (int i = 0; i < myArray.length; i++) {
            if (i < myArray.length - 1) {
                System.out.print(myArray[i] + ", ");
            } else {
                System.out.print(myArray[i]);
            }
        }
        System.out.println("]");

        //suma
        double total = 0;
        for (int i = 0; i < myArray.length; i++) {
            total += myArray[i];
        }
        System.out.println("total: " + total);

        //encontrar el elemento mas grande
        double max = myArray[0];
        int indexOfMax = 0;
        for (int i = 1; i < myArray.length; i++) {
            if (myArray[i] > max) {
                max = myArray[i];
                indexOfMax = i;
            }
        }
        System.out.println("El elemento mas grande es: " + max + "\nEn la posición: " + indexOfMax);

        //encontrar el elemento mas pequeño
        double min = myArray[0];
        int indexOfMin = 0;
        for (int i = 1; i < myArray.length; i++) {
            if (myArray[i] < min) {
                min = myArray[i];
                indexOfMin = i;
            }
        }
        System.out.println("El elemento mas pequeño es: " + min + "\nEn la posición: " + indexOfMin);

        //mezclar
        for (int i = 0; i < myArray.length; i++) {
            //generar un index aleatorio
            int j = (int) (Math.random() * myArray.length);
            //System.out.println("j: " + j);
            double temp = myArray[i];
            myArray[i] = myArray[j];
            myArray[j] = temp;
        }
        System.out.print("[");
        for (int i = 0; i < myArray.length; i++) {
            if (i < myArray.length - 1) {
                System.out.print(myArray[i] + ", ");
            } else {
                System.out.print(myArray[i]);
            }
        }
        System.out.println("]");

        //shifting a la derecha
        //[10, 20, 30, 40 ,50]
        //[50, 10, 20, 30 ,40]
        double temp = myArray[myArray.length - 1];
        for (int i = myArray.length - 2; i >= 0; i--) {
            myArray[i + 1] = myArray[i];
        }
        myArray[0] = temp;
        System.out.print("[");
        for (int i = 0; i < myArray.length; i++) {
            if (i < myArray.length - 1) {
                System.out.print(myArray[i] + ", ");
            } else {
                System.out.print(myArray[i]);
            }
        }
        System.out.println("]");

        String[] months = {"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"};
        java.util.Scanner input = new java.util.Scanner(System.in);
        System.out.println("Ingrese un número del mes (1 al 12): ");
        int monthNumber = input.nextInt();
        System.out.println("El mes es : " + months[monthNumber - 1]);
    }
}
