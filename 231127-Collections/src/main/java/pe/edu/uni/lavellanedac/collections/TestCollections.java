/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package pe.edu.uni.lavellanedac.collections;

import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author Luis Avellaneda <luis.avellaneda.c@uni.pe>
 */
public class TestCollections {

    public static void main(String[] args) {
        System.out.println("TestCollections!");
        String[] cities = {"Arequipa", "Lima", "Piura", "Oxapampa", "Callao", "Junin", "Iquitos"};
        ArrayList<String> collection1 = new ArrayList<>();
        collection1.add(cities[0]);
        collection1.add(cities[1]);
        collection1.add(cities[2]);
        collection1.add(cities[3]);
        System.out.println("Una lista de ciudades: " + collection1);

        System.out.println("La ciudad " + cities[4] + " está presente en la lista? ");
        if (collection1.contains(cities[4])) {
            System.out.println("La ciudad " + cities[4] + " está contenida!");
        } else {
            System.out.println("La ciudad " + cities[4] + " no está contenida!");
        }
        int i = 4;
        if (collection1.remove(cities[i])) {
            System.out.println(cities[i] + " ha sido removido");
        } else {
            System.out.println(cities[i] + " no ha sido removido");
        }
        i = 3;
        if (collection1.remove(cities[i])) {
            System.out.println(cities[i] + " ha sido removido");
        } else {
            System.out.println(cities[i] + " no ha sido removido");
        }
        System.out.println(collection1.size() + " ciudades presentes");

        Collection<String> collection2 = new ArrayList<>();
        collection2.add(cities[4]);
        collection2.add(cities[5]);
        collection2.add(cities[6]);
        collection2.add(cities[1]);

        System.out.println("Lista de ciudades en collection2: " + collection2);

        ArrayList<String> c1;
        c1 = (ArrayList<String>) collection1.clone();
        c1.addAll(collection2);
        System.out.println("Ciudades en collection1 o collection2: " + c1);

        c1 = (ArrayList<String>) collection1.clone();
        c1.retainAll(collection2);
        System.out.println("Ciudades en collection1 y collection2: " + c1);

        c1 = (ArrayList<String>) collection1.clone();
        c1.removeAll(collection2);
        System.out.println("Ciudades de collection1 menos collection2: " + c1);

    }
}
