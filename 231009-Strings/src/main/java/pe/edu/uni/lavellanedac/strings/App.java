/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package pe.edu.uni.lavellanedac.strings;

/**
 *
 * @author Luis Avellaneda <luis.avellaneda.c@uni.pe>
 */
public class App {

    public static void main(String[] args) {
        String x = " muy buenas tardes con todos ";
        System.out.println(x);

        // Length
        System.out.println("length of string: " + x.length());

        // Character location
        System.out.println(x.charAt(0));
        System.out.println(x.charAt(x.length() - 1));

        //Convert
        System.out.println(x.toUpperCase());

        //split
        String[] partes = x.split(" ");
        for (int i = 0; i < partes.length; i++) {
            System.out.println(partes[i]);
        }

        //trim
        String y = x.trim();
        System.out.println("trim:" + y);
        System.out.println("lenght of trim:" + y.length());
    }
}
